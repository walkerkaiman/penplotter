void drawToDoList() {  
  // Erase all painting on main image background, and draw the existing "ToDo" list
  // on the off-screen buffer.

  int j = ToDoList.length;
  float x1, x2, y1, y2, brightness;

  if ((indexDrawn + 1) < j) {
    // Ready the offscreen buffer for drawing onto
    offScreen.beginDraw();

    if (indexDrawn < 0) {
      offScreen.noFill();
      offScreen.strokeWeight(1);
      offScreen.stroke(LIGHT_RED);
      offScreen.rect(float(CANVAS_LEFT), float(CANVAS_TOP), CANVAS_WIDTH, CANVAS_HEIGHT);
    } else {
      offScreen.image(imgMain, 0, 0);
    }

    offScreen.strokeWeight(1); 

    brightness = 0;
    color DoneColor = lerpColor(PEN_COLOR, WHITE, brightness);

    brightness = 0.8;
    color ToDoColor = lerpColor(PEN_COLOR, WHITE, brightness); 

    x1 = 0;
    y1 = 0;

    boolean virtualPenDown = false;

    int index = 0;

    if (index < 0) {
      index = 0;
    }

    while (index < j) {
      PVector toDoItem = ToDoList[index];

      x2 = toDoItem.x;
      y2 = toDoItem.y;

      if (x2 >= 0) {
        if (virtualPenDown) {
          if (index < indexDone) {
            offScreen.stroke(DoneColor);
          } else {
            offScreen.stroke(ToDoColor);
          }

          offScreen.line(x1, y1, x2, y2); // Preview lines that are not yet on paper

          x1 = x2;
          y1 = y2;
        } else {
          x1 = x2;
          y1 = y2;
        }
      } else {
        int x3 = -1 * round(x2);

        if (x3 == 30) { // Pen Up
          virtualPenDown = false;
        } else if (x3 == 31) {  // Pen Down
          virtualPenDown = true;
        } else if (x3 == 35) { // Go Home
          x1 = 0;
          y1 = 0;
        }
      }

      index++;
    }

    offScreen.endDraw();
    imgMain = offScreen.get(0, 0, offScreen.width, offScreen.height);
  }
}