PFont font_ML16, font_CB; 

// GUI BUTTONS
SimpleButton pauseButton, brushUpButton, brushDownButton, parkButton, motorOffButton, motorZeroButton, clearButton;
SimpleButton replayButton, quitButton, brushLabel, motorLabel, UIMessage;

void initGUI() {
  
  font_ML16 = loadFont("Miso-Light-16.vlw");
  font_CB = loadFont("Miso-20.vlw");
  
  // BEGIN INITIALIZING GUI ELEMENTS
  PVector tempPos = new PVector(CANVAS_LEFT + 100, CANVAS_BOTTOM + 20);

  pauseButton = new SimpleButton("Start", (int)tempPos.x, CANVAS_BOTTOM + 20, font_CB, 20, TEXT_COLOR, TEXT_HIGHLIGHT);
  tempPos.x += 60; 

  brushLabel = new SimpleButton("Pen:", (int)tempPos.x, (int)tempPos.y, font_CB, 20, LABEL_COLOR, LABEL_COLOR);
  tempPos.x += 45;

  brushUpButton = new SimpleButton("Up", (int)tempPos.x, (int)tempPos.y, font_CB, 20, TEXT_COLOR, TEXT_HIGHLIGHT);
  tempPos.x += 22;

  brushDownButton = new SimpleButton("Down", (int)tempPos.x, (int)tempPos.y, font_CB, 20, TEXT_COLOR, TEXT_HIGHLIGHT);
  tempPos.x += 44;

  parkButton = new SimpleButton("Park", (int)tempPos.x, (int)tempPos.y, font_CB, 20, TEXT_COLOR, TEXT_HIGHLIGHT);
  tempPos.x += 60;

  motorLabel = new SimpleButton("Motors:", (int)tempPos.x, (int)tempPos.y, font_CB, 20, LABEL_COLOR, LABEL_COLOR);
  tempPos.x += 55;

  motorOffButton = new SimpleButton("Off", (int)tempPos.x, (int)tempPos.y, font_CB, 20, TEXT_COLOR, TEXT_HIGHLIGHT);
  tempPos.x += 30;

  motorZeroButton = new SimpleButton("Zero", (int)tempPos.x, (int)tempPos.y, font_CB, 20, TEXT_COLOR, TEXT_HIGHLIGHT);
  tempPos.x += 70;

  clearButton = new SimpleButton("Clear All", (int)tempPos.x, CANVAS_BOTTOM + 20, font_CB, 20, TEXT_COLOR, TEXT_HIGHLIGHT);
  tempPos.x += 80;

  replayButton = new SimpleButton("Replay All", (int)tempPos.x, CANVAS_BOTTOM + 20, font_CB, 20, TEXT_COLOR, TEXT_HIGHLIGHT);
  tempPos.x = CANVAS_LEFT + 30;   
  tempPos.y = 30;

  quitButton = new SimpleButton("Quit", (int)tempPos.x, (int)tempPos.y, font_CB, 20, LABEL_COLOR, TEXT_HIGHLIGHT); 
  tempPos.x = 655;
  
  // END INITIALIZING GUI ELEMENTS
  
  rectMode(CORNERS);
  background(WHITE);
}