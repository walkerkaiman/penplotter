// ========================================== USER CHANGEABLE SETTINGS ========================================== //

// HARDWARE PARAMETERS
final int RAISE_PERCENT = 70; // Brush UP position. Higher number lifts higher. 
final int DOWN_PERCENT = 30;    // Brush DOWN position. Higher number lifts higher. 
final int RAISING_DELAY_MS = 300; // Delay (ms) after raising.
final int LOWER_DELAY_MS = 300; // Delay (ms) after raising.

// IMAGE PARAMETERS
final String IMAGE_FILE_NAME = "1.jpg";
final int SAMPLE_STEP = 2; // Higher number is lower resolution.



// ========================================== DO NOT CHANGE THE FOLLOWING ========================================== //

// LIBRARIES
import de.looksgood.ani.*;
import processing.serial.*;

// SERIAL DEVICE (AxiDraw3 on OSX)
Serial myPort;

// POSITIONS
final float MOTOR_SPEED = 4000.0;  // Steps per second
final int FPS = 60;
final float MOTOR_STEPS_PER_PIXEL = 32.1;
final float PIXELS_PER_INCH = 63.3; 
final float CANVAS_WIDTH = PIXELS_PER_INCH * 11.0;
final float CANVAS_HEIGHT = PIXELS_PER_INCH * 8.5;
final int CANVAS_LEFT = 30;
final int CANVAS_RIGHT = round(CANVAS_LEFT + PIXELS_PER_INCH * 11.0);
final int CANVAS_TOP = 62;
final int CANVAS_BOTTOM = round(CANVAS_TOP + PIXELS_PER_INCH * 8.5);
final int CANVAS_REST_Y = 6;
final int MOTOR_MIN_X = 0;
final int MOTOR_MIN_Y = 0;
final int MOTOR_MAX_X = int(floor(float(CANVAS_RIGHT - CANVAS_LEFT) * MOTOR_STEPS_PER_PIXEL)-1);
final int MOTOR_MAX_Y = int(floor(float(CANVAS_BOTTOM - CANVAS_TOP) * MOTOR_STEPS_PER_PIXEL)-1);
final int MOTOR_UP_POS = 7500 + 175 * RAISE_PERCENT;    // Brush UP position, native units
final int MOTOR_DOWN_POS = 7500 + 175 * DOWN_PERCENT;   // Brush DOWN position, native units.

// COLORS
final color BLACK = color(0);
final color WHITE = color(255);
final color DARK_GRAY = color(25, 25, 25);
final color LIGHT_RED = color(255, 128, 128);
final color PEN_COLOR = BLACK;
final color TEXT_HIGHLIGHT = DARK_GRAY;
final int TEXT_COLOR = 75;
final int LABEL_COLOR = 150;
final int DEFOCUS_COLOR = 175;

// FLAGS
boolean reverseMotorX = false;
boolean reverseMotorY = false;
boolean isBrushDown = false;
boolean isBrushDownAtPause = false;
boolean isSerialOnline = false;
boolean doSerialConnect = true;
boolean isPaused = true;
boolean isFirstPath = true;

// DRAWING ELEMENTS
PGraphics offScreen;
PImage imgMain, imgLocator, imgButtons, imgHighlight, sampledImg;

int xLocAtPause, yLocAtPause, MotorX, MotorY, MotorLocatorX, MotorLocatorY;
int serialIncoming = 0;
int lastButtonUpdateX = 0;
int lastButtonUpdateY = 0;
int lastX_DrawingPath = 0;
int lastY_DrawingPath = 0;
int NextMoveTime = 0;
int UIMessageExpire = 0;
int PaintDest = 0;
int indexDone = -1;
int indexDrawn = -1;
int raiseBrushStatus = -1;
int SubsequentWaitTime = -1;
int lowerBrushStatus = -1;
int moveStatus = -1;
int MoveDestX = -1;
int MoveDestY = -1;
PVector queuePt1 = new PVector(-1, -1);
PVector queuePt2 = new PVector(-1, -1);
PVector lastPosition = new PVector(-1, -1);

void setup() {
  
  size(800, 631, P2D);
  surface.setTitle("Pen Plotter");
  frameRate(FPS);  // Sets maximum FPS

  Ani.init(this);
  Ani.setDefaultEasing(Ani.LINEAR);
  
  addTask(CMD_HOME); 

  int[] pos = getMotorPixelPos();

  MotorLocatorX = pos[0];
  MotorLocatorY = pos[1];
  
  offScreen = createGraphics(width, height);
  sampledImg = loadImage(IMAGE_FILE_NAME);
  sampledImg.resize((int)CANVAS_WIDTH, (int)CANVAS_HEIGHT);
  initGUI();
  NextMoveTime = millis();
  drawToDoList();
  redrawButtons();
  redrawHighlight();
  redrawLocator();
}

void draw() {
  
  drawToDoList();

  // NON-DRAWING LOOP CHECKS ==========================================
  if (!doSerialConnect) {
    checkServiceBrush();
  }

  checkHighlights();

  // ALL ACTUAL DRAWING ==========================================
  image(imgMain, 0, 0, width, height); // Draw Background image  (incl. paint paths)
  image(imgButtons, 0, 0); // Draw buttons image
  image(imgHighlight, 0, 0); // Draw highlight image
  image(imgLocator, MotorLocatorX - 10, MotorLocatorY - 15); // Draw locator crosshair at xy pos, less crosshair offset

  if (doSerialConnect) {
    // FIRST RUN ONLY
    doSerialConnect = false;
    scanSerial();

    if (isSerialOnline) {    
      myPort.write("EM,1,1\r");  // Configure both steppers in 1/16 step mode

      // Configure brush lift servo endpoints and speed
      myPort.write("SC,4," + str(MOTOR_DOWN_POS) + "\r");  // Brush DOWN position, for painting
      myPort.write("SC,5," + str(MOTOR_UP_POS) + "\r");  // Brush UP position 
      myPort.write("SC,10,65535\r"); // Set brush raising and lowering speed.

      // Ensure that we actually raise the brush:
      isBrushDown = true;  
      raiseBrush();    
      redrawButtons();
    } else { 
      println("Now entering offline simulation mode.\n");
      
      UIMessageExpire = millis() + 5000;
      redrawButtons();
    }
  }
}