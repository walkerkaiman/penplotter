void pause() {
  
  pauseButton.displayColor = TEXT_COLOR;

  if (isPaused) {
    isPaused = false;
    pauseButton.label = "Pause";
    println("Currently Plotting");
    
    if (isBrushDownAtPause) {
      int waitTime = NextMoveTime - millis();

      if (waitTime > 0) { 
        delay (waitTime);  // Wait for prior move to finish:
      }

      if (isBrushDown) { 
        raiseBrush();
      }

      waitTime = NextMoveTime - millis();

      if (waitTime > 0) { 
        delay (waitTime);  // Wait for prior move to finish:
      }

      MoveToXY(xLocAtPause, yLocAtPause);
      waitTime = NextMoveTime - millis();

      if (waitTime > 0) { 
        delay (waitTime);  // Wait for prior move to finish:
      }

      lowerBrush();
    }
  } else {
    isPaused = true;
    pauseButton.label = "Resume";
    println("Currently Paused");

    if (isBrushDown) {
      isBrushDownAtPause = true; 
      raiseBrush();
    } else {
      isBrushDownAtPause = false;
    }

    xLocAtPause = MotorX;
    yLocAtPause = MotorY;
  }

  redrawButtons();
}