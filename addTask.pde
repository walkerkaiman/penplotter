public PVector[] ToDoList = new PVector[0];  // Coordinate/command

// MACHINE COMMANDS
final PVector CMD_DOWN = new PVector(-31, 0); // Command 31 (Lower pen)
final PVector CMD_UP = new PVector(-30, 0); // Command 30 (Raise pen)
final PVector CMD_HOME = new PVector(-35, 0); // Command 35 (Home Position)

void addTask(PVector task){
  ToDoList = (PVector[]) append(ToDoList, task);
}