boolean forceRedraw, shiftKeyDown, keyup, keyright, keyleft, keydown = false;

void keyReleased() {
  
  if (key == CODED) {
    if (keyCode == UP) { 
      keyup = false;
    }
    if (keyCode == DOWN) { 
      keydown = false;
    } 
    if (keyCode == LEFT) { 
      keyleft = false;
    }
    if (keyCode == RIGHT) { 
      keyright = false;
    }
    if (keyCode == SHIFT) { 
      shiftKeyDown = false;
    }
  } else {
    key = Character.toLowerCase(key);
  }
}


void keyPressed() {
  
  if (key == CODED) {
    // Arrow keys are used for nudging, with or without shift key.
    if (keyCode == UP) { 
      keyup = true;
    }
    if (keyCode == DOWN) { 
      keydown = true;
    }
    if (keyCode == LEFT) { 
      keyleft = true;
    }
    if (keyCode == RIGHT) { 
      keyright = true;
    }
    if (keyCode == SHIFT) { 
      shiftKeyDown = true;
    }
  } else {
    key = Character.toLowerCase(key);
    println("Key pressed: " + key); 

    if ( key == 'b') {
      if (isBrushDown) {
        raiseBrush();
      } else {
        lowerBrush();
      }
    }
    if ( key == 'z') { 
      zero();
    }
    if ( key == 'c') { 
      clearall();
    }
    if ( key == ' ') { 
      pause();
    }
    if ( key == 't') { 
      MotorsOff();
    }
    if ( key == 'p') { 
      ArrayList<PVector> points = getPointsFromImage(sampledImg, 100);
      
      offScreen.beginDraw();// Begin draw points
      
      offScreen.noFill();
      offScreen.strokeWeight(1);
      offScreen.stroke(LIGHT_RED);
      offScreen.rect(float(CANVAS_LEFT), float(CANVAS_TOP), CANVAS_WIDTH, CANVAS_HEIGHT);
      
      offScreen.background(WHITE);
      offScreen.stroke(BLACK); 
      offScreen.strokeWeight(2);
        
      for(PVector p : points){
        offScreen.point(p.x, p.y);
      }

      offScreen.endDraw(); // End draw points
      imgMain = offScreen.get(0, 0, offScreen.width, offScreen.height);
      
      image(imgMain, 0, 0, width, height);
      
      // Make sure you're not dragging across the paper
      raiseBrush();
      delay(RAISING_DELAY_MS);
      
      // Plot the points (move, lower, raise)
      for(PVector p : points){
        int x = (int)p.x;
        int y = (int)p.y;
        int motorX = (int)(x * MOTOR_STEPS_PER_PIXEL);
        int motorY = (int)(y * MOTOR_STEPS_PER_PIXEL);
        
        MoveToXY(motorX, motorY);
        println("X: " + x + ", Y: " + y);
        
        lowerBrush();
        delay(LOWER_DELAY_MS);
        
        raiseBrush();
        delay(RAISING_DELAY_MS);
      }
    }
    
    if ( key == 'q') {
      raiseBrush();
      MoveToXY(0, 0);
    }
  }
}


void mousePressed() {
  
  boolean doHighlightRedraw = false;

  //The mouse button was just pressed!  Let's see where the user clicked!
  if ((mouseX >= CANVAS_LEFT) && (mouseX <= CANVAS_RIGHT) && (mouseY >= CANVAS_TOP) && (mouseY <= CANVAS_BOTTOM)) { 
    doHighlightRedraw = true;
  }

  if (doHighlightRedraw) {
    redrawLocator();
    redrawHighlight();
  }

  if (pauseButton.isSelected()) { 
    pause();
  } else if (brushUpButton.isSelected()) {
    if (isPaused) {
      raiseBrush();
    } else {     
      addTask(CMD_UP);
    }
  } else if (brushDownButton.isSelected()) {
    if (isPaused) {
      lowerBrush();
    } else {
      addTask(CMD_DOWN);
    }
  } else if (parkButton.isSelected()) {
    if (isPaused) { 
      raiseBrush();
      MoveToXY(0, 0);
    } else {
      addTask(CMD_UP);
      addTask(CMD_HOME);
    }
  } else if (motorOffButton.isSelected()) {
    MotorsOff();
  } else if (motorZeroButton.isSelected()) {
    zero();
  } else if (clearButton.isSelected()) {
    clearall();
  } else if (replayButton.isSelected()) {
    // Clear indexDone to "zero" (actually, -1, since even element 0 is not "done.")   & redraw to-do list.

    indexDone = -1;    // Index in to-do list of last action performed
    indexDrawn = -1;   // Index in to-do list of last to-do element drawn to screen
    drawToDoList();
  } else if ( quitButton.isSelected() ) {
    quitApp();
  }
}