final int MARGIN = 20;

ArrayList<PVector> getPointsFromImage(PImage image, float threshold) {
  ArrayList<PVector> points = new ArrayList<PVector>();

  for (int y = CANVAS_TOP+MARGIN; y < CANVAS_BOTTOM-MARGIN; y += SAMPLE_STEP) {
    for (int x = CANVAS_LEFT+MARGIN; x < CANVAS_RIGHT-MARGIN; x += SAMPLE_STEP) {
      float pixelBrightness = brightness(image.get(x, y));

      if (pixelBrightness > threshold) {
        PVector pos = new PVector(x, y);
        points.add(pos);
      }
    }
  }
  return points;
}